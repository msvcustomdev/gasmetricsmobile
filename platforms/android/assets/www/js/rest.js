var urlLocations = "http://localhost:8080/gasmetricsbrod/rest/location/";
var urlTypes = "http://localhost:8080/gasmetricsbrod/rest/types";

function fillLocations() {
	$.getJSON(urlLocations, function(data) {
		$("#locationsList").empty();
		var locations = [];
		$.each(data, function(key, val) {
			locations.push("<li><a href='#values' id='locationId-" + val.mdLocationId + "'>" + val.locationName + "</a></li>");
		})
		$("#locationsList").append(locations);
		$("#locationsList li").click(function(event) {
			fillValues($(this).attr("id").substring(11));
		})
		$("#locationsList").listview("refresh");
	})
}

function fillValues(selectedLocation) {
	var urlValues = urlLocations + selectedLocation + "/values";
	$.getJSON(urlValues, function(data) {
		$("#valuesTable").empty();
		var values = [];
		$.each(data, function(key, val) {
			values.push("<div class='ui-block-a'>Naziv</div><div class='ui-block-b'>" + val.measureConcentration + " " + "MJ"
					+ "</div><div class='ui-block-c'>" + val.measureDate + "(" + val.measureTime + ")" + "</div>");
		})
		$("#valuesTable").append(values);
	})
}

function fillTypesInfo() {
	$.getJSON(urlTypes, function(data) {
		$("#borderValuesTable").empty();
		var borderValues = [];
		$.each(data, function(key, val) {
			borderValues.push("<div class='ui-block-a'>" + val.typeName + "</div><div class='ui-block-b'></div><div class='ui-block-a'>"
					+ val.typeLimitBorder + "</div><div class='ui-block-b'>" + val.borderDescription + "</div><div class='ui-block-a'>"
					+ val.typeLimitCritical + "</div><div class='ui-block-b'>" + val.criticalDescription + "</div><div class='ui-block-a'>"
					+ val.typeLimitWarning + "</div><div class='ui-block-b'>" + val.warningDescription + "</div>");
		})
		$("#borderValuesTable").append(borderValues);
	})
}